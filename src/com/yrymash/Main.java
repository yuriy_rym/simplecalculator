package com.yrymash;

import com.yrymash.utils.Utils;

public class Main {
		
	static Integer firstElement;
	static Integer secondElement;
    static Integer result = 0;
    static Integer finalResult = 0;
    static Integer tempResult = 0;
    static String operand = new String();
    static String element;
    static String firstElementAsString = new String();
    static String secondElementAsString = new String();
    static String onlyElementAsString = new String();
    static OperandAndItPosition operandAndItPosition = new OperandAndItPosition();
    
   		
	 
    //TODO generate Exception (no first, operand, second)
	
	/**
	 * Method finds element in String from given position
	 * @param startPosition - index of start position
	 * @param str - given String for search
	 * @return number from String starting from start position to operand
	 */
    public static Integer findElementInStr(int startPosition, String str){
		Integer element;
		String elementAsString = new String();
		if(startPosition == 0 && str.charAt(0) == '-'){
			elementAsString += Character.toString(str.charAt(0));
		}
		for(int i=startPosition; i < str.length(); i++){
			if(!Utils.isCharANumber(str.charAt(i)) && !(i == 0 && str.charAt(0) == '-'))break;
			if(Utils.isCharANumber(str.charAt(i))){ // TODO or for negative first element
				elementAsString += Character.toString(str.charAt(i));
			}	
		}
		
		element = Integer.parseInt(elementAsString);
		return element;
		}
	
    /**
     * Method finds operand in String from given position
     * @param startPositionForFindOperand - index of start position
     * @param str - String given for search
     * @return - operand and index of it position from starting position to the end of String
     */
    
	public static OperandAndItPosition findOperand(int startPositionForFindOperand, String str){
		boolean isOperandInStr = false;
		for(int i=startPositionForFindOperand; i < str.length(); i++){
			if(Utils.isOperand(str.charAt(i))){
    			operand = Character.toString(str.charAt(i));
    			operandAndItPosition = new OperandAndItPosition(operand, i);
    			isOperandInStr = true;
    			break;
				}
		}
		if(isOperandInStr == true){
			return operandAndItPosition;
		}else{
			return null;
		}
	}
	
	static class OperandAndItPosition {
		String operandName;
		Integer operandPosition;
		
		public OperandAndItPosition(){
		}
		
		public OperandAndItPosition(String operandName, Integer operandPosition){
			this.operandName = operandName;
			this.operandPosition = operandPosition;
		}
		
	}
	
	/**
	 * Method adds or subtracts elements from String if there is more than one element in String
	 * @param str - String with operations for calculation
	 * @return sum or difference of elements from String
	 */
	
    public static Integer callculationElements(String str){
    	if(findOperand(1, str)== null){
    		return result;
    	}else{
    			firstElement = findElementInStr(0, str);
    			operandAndItPosition = findOperand(1, str);
    			Integer z = operandAndItPosition.operandPosition;
    			secondElement = findElementInStr(z+1, str);
    		if(operand.equals("+")){
    			tempResult = firstElement + secondElement;
    		} else {
    			tempResult = firstElement - secondElement;
    		}
    		result = tempResult;
    	}
    		str = str.replace(firstElement.toString() + operand.toString() + secondElement.toString(), result.toString());
    		callculationElements(str);
    	// TODO get firstElement from str to variable firstElement
    	// TODO get operand from str to variable oprand
    	// TODO get secondElement from str to variable secondElement
    	// TODO callculate result of firstElement operand secondElement  to variable tempResult
    	// TODO result = tempResult
    	// TODO put tempResult instead of firstElenent operand secondElement into begin of str
    	// TODO call callculation(str);
    	//}
    	return result; // result not null?
    }
    
    /**
     * Method adds or subtracts elements from any String
     * @param str - String with operations for calculation
     * @return sum or difference of elements from String; the same element if there is one element in String 
     * @throws Exception has been thrown if there is char different from number and operand in String
     */
    
    public static Integer callculation(String str)throws Exception{
    	for(int i=0; i < str.length(); i++){
			if(!Utils.isCharANumber(str.charAt(i)) && !Utils.isOperand(str.charAt(i))){
				throw new Exception("Incorrect element");// TODO Exception unreal number, -10
			}
		}
			if(findOperand(1, str)== null){
				finalResult = findElementInStr(0, str);			
    	}else{
    		finalResult = callculationElements(str);
    	}
    	return finalResult;
    }
	
    public static void main(String[] args) {
    	try{
    	System.out.println(callculation(args[0]));
        }catch (Exception e){
        System.out.println("Incorrect element");
         }
    }
}



